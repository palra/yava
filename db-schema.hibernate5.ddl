drop table if exists classe;
drop table if exists classe_matiere;
drop table if exists eleve;
drop table if exists evaluation;
drop table if exists matiere;
drop table if exists responsable;
drop table if exists users;
drop table if exists Utilisateur_roles;
create table classe (id integer not null auto_increment, anneePromotion integer not null, nom varchar(255), primary key (id)) ;
create table classe_matiere (Classe_id integer not null, matieres_id integer not null) ;
create table eleve (id integer not null auto_increment, allergies varchar(255), dateInscription datetime, etablissementPrecedent varchar(255), code_postal varchar(255), email varchar(255), nom varchar(255), nom_rue varchar(255), numero_rue varchar(255), prenom varchar(255), sexe char(1), telephone_domicile varchar(255), telephone_portable varchar(255), ville varchar(255), medecinTraitant varchar(255), remarquesMedicales varchar(255), telephoneMedecinTraitant varchar(255), classe_id integer not null, primary key (id)) ;
create table evaluation (id integer not null auto_increment, note float not null, eleve_id integer not null, matiere_id integer not null, primary key (id)) ;
create table matiere (id integer not null auto_increment, nom varchar(255), primary key (id)) ;
create table responsable (id integer not null auto_increment, code_postal varchar(255), email varchar(255), nom varchar(255), nom_rue varchar(255), numero_rue varchar(255), prenom varchar(255), sexe char(1), telephone_domicile varchar(255), telephone_portable varchar(255), ville varchar(255), eleve_id integer not null, primary key (id)) ;
create table users (id integer not null auto_increment, email varchar(255), password varchar(255), primary key (id)) ;
create table Utilisateur_roles (Utilisateur_id integer not null, roles varchar(255) not null) ;
alter table classe_matiere add constraint FK8h7x8yu3iqc97yyls3udfg592 foreign key (matieres_id) references matiere (id);
alter table classe_matiere add constraint FK2n82xe6e2cj1iycrlcd4swtgt foreign key (Classe_id) references classe (id);
alter table eleve add constraint FK8nrji46bsbcoohx749w9j78yx foreign key (classe_id) references classe (id);
alter table evaluation add constraint FKr84sr1wl8siktoprkut6oitn8 foreign key (eleve_id) references eleve (id);
alter table evaluation add constraint FKr0i9fijfxr3pubdv8kfg7i07h foreign key (matiere_id) references matiere (id);
alter table responsable add constraint FK8foq6a53bm3uuxvlejphi3w42 foreign key (eleve_id) references eleve (id);
alter table Utilisateur_roles add constraint FKdn2phdw0cpuaf1pbmnapt0vea foreign key (Utilisateur_id) references users (id);

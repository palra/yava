/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.palra.gestion_ecole;

import javax.swing.SwingUtilities;

/**
 *
 * @author palra
 */
public class Main {
    public static void main(String[] args) {
        // invokeLater permet de ne pas bloquer le thread principal en cas de 
        // blocage dans l'exécution de Application.
        SwingUtilities.invokeLater(() -> {
            Application app = new Application();
        });
    }
    
}

/*
 * Copyright (C) 2017 palra
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xyz.palra.gestion_ecole.controller;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import xyz.palra.gestion_ecole.ViewManager;
import xyz.palra.gestion_ecole.dao.ClasseDAO;
import xyz.palra.gestion_ecole.dao.EleveDAO;
import xyz.palra.gestion_ecole.dao.EvaluationDAO;
import xyz.palra.gestion_ecole.dao.MatiereDAO;
import xyz.palra.gestion_ecole.mock.ClasseMockData;
import xyz.palra.gestion_ecole.model.Classe;
import xyz.palra.gestion_ecole.model.Eleve;
import xyz.palra.gestion_ecole.model.Evaluation;
import xyz.palra.gestion_ecole.model.Matiere;
import xyz.palra.gestion_ecole.security.NonAuthentifieException;
import xyz.palra.gestion_ecole.view.ListeClasseView;
import xyz.palra.gestion_ecole.view.ListeEleveView;
import xyz.palra.gestion_ecole.view.ListeMatiereView;
import xyz.palra.gestion_ecole.view.ShowClasseView;
import xyz.palra.gestion_ecole.view.ShowMatiereView;
import xyz.palra.gestion_ecole.view.ShowEleveView;
import xyz.palra.gestion_ecole.view.ShowEleveViewFactory;
import xyz.palra.gestion_ecole.view.ShowMatiereViewFactory;
import xyz.palra.gestion_ecole.view.event.ClassSelectListener;
import xyz.palra.gestion_ecole.view.event.EvalCreateListener;

/**
 * Contrôleur de la liste des classes
 * @author palra
 */
public class GestionEcoleController extends AbstractController {
    public ListeClasseView listeClasseAction() {        
        ClasseDAO cDAO = app.getDAOFactory().getClasseDAO();
        
        List<Classe> list = cDAO.findAll();
        ListeClasseView view = new ListeClasseView(list);
        
        ViewManager vm = this.getViewManager();
        
        view.addClassSelectListener((Classe classe) -> {
            JPanel view1 = this.viewClasseAction(classe);
            vm.push(view1);
        });
        
        return view;
    }
    
    public ShowClasseView viewClasseAction(Classe classe) {
        ShowClasseView view = new ShowClasseView(classe);
        
        ViewManager vm = this.getViewManager();
        
        view.addReturnListener((ActionEvent ae) -> {
            vm.pop();
        });
        
        view.setListeEleveView(
                this.listeEleveAction(classe)
        );
        
        view.setListeMatieresView(
                this.listeMatiereAction(classe)
        );
        
        return view;
    }
    
    public ListeEleveView listeEleveAction(Classe classe) {
        ListeEleveView view = new ListeEleveView(classe.getEleves());
        view.setViewFactory((Eleve eleve) -> {
            try {
                return this.showEleveAction(eleve);
            } catch (NonAuthentifieException ex) {
                Logger.getLogger(GestionEcoleController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            return null;
        });
        
        return view;
    }
    
    public ShowEleveView showEleveAction(Eleve eleve) throws NonAuthentifieException {
        EleveDAO eDAO = app.getDAOFactory().getEleveDAO();
        ShowEleveView view = new ShowEleveView(eleve, GestionEcoleController.this.getSecurityManager().isGranted("ROLE_ADMIN"));
        
        view.addEventListener((Eleve e) -> {
            eDAO.update(e);
            JOptionPane.showMessageDialog(null, "Élève enregistré");
        });
        
        return view;
    }
    
    public ListeMatiereView listeMatiereAction(Classe classe) {
        ListeMatiereView view = new ListeMatiereView(classe, classe.getMatieres());
        view.setViewFactory((Classe classe1, Matiere matiere) -> {            
            try {
                return this.showMatiereAction(classe1, matiere);
            } catch (NonAuthentifieException ex) {
                Logger.getLogger(GestionEcoleController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            return null;
        });
        
        return view;
    }
    
    
    public ShowMatiereView showMatiereAction(Classe classe, Matiere matiere) throws NonAuthentifieException {
        EvaluationDAO mDAO = app.getDAOFactory().getEvaluationDAO();
        ShowMatiereView view = new ShowMatiereView(classe, matiere, GestionEcoleController.this.getSecurityManager().isGranted("ROLE_ADMIN"));
        
        view.addEvalCreateListener((Map<Eleve,String> notes) -> {
            for(Map.Entry<Eleve, String> entry : notes.entrySet()) {
                Eleve key = entry.getKey();
                String value = entry.getValue();

                if(!value.equals("")) {
                    Evaluation evaluation = new Evaluation();
                    evaluation.setMatiere(matiere);
                    evaluation.setEleve(key);
                    evaluation.setNote(Float.parseFloat(value));
                    mDAO.create(evaluation);
                }
            }
            
            view.initNotes();
            JOptionPane.showMessageDialog(null, "Évaluation crée");
        });
        
        return view;
    }
    
}

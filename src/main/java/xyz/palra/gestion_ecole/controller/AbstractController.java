/*
 * Copyright (C) 2017 palra
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xyz.palra.gestion_ecole.controller;

import xyz.palra.gestion_ecole.Application;
import xyz.palra.gestion_ecole.ApplicationAware;
import xyz.palra.gestion_ecole.ViewManager;
import xyz.palra.gestion_ecole.security.SecurityManager;

/**
 *
 * @author palra
 */
public abstract class AbstractController implements ApplicationAware {
    protected Application app;
    
    @Override
    public void setApplication(Application app) {
        this.app = app;
    }
    
    public ViewManager getViewManager() {
        return this.app.getViewManager();
    }
    
    public SecurityManager getSecurityManager() {
        return this.app.getSecurityManager();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.palra.gestion_ecole.controller;

import xyz.palra.gestion_ecole.view.LoginView;

/**
 *
 * @author palra
 */
public class SecurityController extends AbstractController {
    public LoginView loginAction() {
        LoginView view = new LoginView();
        
        view.addLoginListener((String email, String password) -> {
            if(!this.getSecurityManager().login(email, password)) {
                view.notifyLoginError();
            }
        });
        
        return view;
    }
}

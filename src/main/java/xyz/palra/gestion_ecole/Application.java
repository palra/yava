/*
 * Copyright (C) 2017 palra
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xyz.palra.gestion_ecole;

import java.awt.event.ActionEvent;
import java.util.EnumSet;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.schema.TargetType;
import xyz.palra.gestion_ecole.controller.GestionEcoleController;
import xyz.palra.gestion_ecole.controller.SecurityController;
import xyz.palra.gestion_ecole.dao.DAOFactory;
import xyz.palra.gestion_ecole.repository.UtilisateurRepository;
import xyz.palra.gestion_ecole.security.UtilisateurRepositoryInterface;
import xyz.palra.gestion_ecole.security.SecurityManager;
import xyz.palra.gestion_ecole.security.UtilisateurInterface;
import xyz.palra.gestion_ecole.view.LoginView;

/**
 * COntrôle l'exécution de l'application, et encapsule les objets partagés à
 * l'ensemble des composants de l'application.
 * @author palra
 */
public class Application {
        
    private DAOFactory daoFactory;
    private ViewManager vm;
    private SessionFactory sessionFactory;
    private Configuration hibernateConfiguration;
    private SecurityManager securityManager;
    
    private static Logger logger = Logger.getLogger(Application.class);
    
    /**
     * Constructeur de l'application.
     * Met en place les services et lance l'exécution de l'application.
     */
    public Application() {
        this.setupLogging();
        this.setupServices();
        this.run();
    }
    
    /**
     * Retourne la DAO Factory de l'application.
     * @return La DAO Factory de l'application.
     */
    public DAOFactory getDAOFactory() {
        return daoFactory;
    }

    /**
     * Retourne le gestionnaire de vue de l'application.
     * @return Le gestionnaire de vue de l'application.
     */
    public ViewManager getViewManager() {
        return vm;
    }

    /**
     * Retourne la SessionFactory de Hibernate.
     * @return La SessionFactory de Hibernate
     */
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    /**
     * Retourne le gestionnaire de sécurité.
     * @return Le gestionnaire de sécurité.
     */
    public SecurityManager getSecurityManager() {
        return securityManager;
    }
    
    /**
     * COnfigure le logging pour l'affichage sur la sortie standard.
     */
    private void setupLogging() {
        Logger root = Logger.getRootLogger();
        ConsoleAppender ca = new ConsoleAppender();
        ca.setName("console");
        ca.setLayout(new PatternLayout("%d [%-6p] %C{1}.%M(%F:%L) - %m%n"));
        ca.activateOptions();
        
        root.addAppender(ca);
        root.setLevel(Level.DEBUG);
    }
    
    /**
     * Construit tous les services de l'application.
     */
    private void setupServices() {
        // Hibernate
        hibernateConfiguration = new Configuration().configure();
        sessionFactory = hibernateConfiguration.buildSessionFactory(); 
        
        // DAO Factory
        daoFactory = new DAOFactory(sessionFactory.openSession());
        
        // SecurityContext
        UtilisateurRepositoryInterface userRepo = new UtilisateurRepository(
                sessionFactory.openSession()
        );
        securityManager = new SecurityManager(userRepo);
    }
    
    private void exit() {
        sessionFactory.close();
        System.exit(0);
    }
    
    private JFrame makeJFrame() {
        return this.makeJFrame(true);
    }
    
    /**
     * Construit une JFrame.
     */
    private JFrame makeJFrame(boolean withMenu) {
        JFrame jFrame = new JFrame();
        jFrame.setTitle("Gestionnaire d'école");
        jFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        jFrame.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                if (JOptionPane.showConfirmDialog(jFrame, 
                    "Êtes-vous sûrs de vouloir fermer l'application ?", "Fermer l'application", 
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                    exit();
                }
            }
        });
        
        if(withMenu) {
            JMenuBar jMenuBar = new JMenuBar();
            JMenu file = new JMenu("Outils");
            JMenuItem genDDL = new JMenuItem("Générer le fichier DDL");
            genDDL.addActionListener((ActionEvent ae) -> {
                SwingUtilities.invokeLater(() -> {
                    String fileName = this.exportDDL();
                    JOptionPane.showMessageDialog(
                            jFrame,
                            "La DDL a été écrite dans le fichier " + fileName,
                            "Générer le fichier DDL",
                            JOptionPane.INFORMATION_MESSAGE
                    );
                });
            });
            file.add(genDDL);
            jMenuBar.add(file);

            jFrame.setJMenuBar(jMenuBar);
        }
        
        return jFrame;
    }
    
    /**
     * Lance l'exécution de l'application.
     */
    private void run() {
        vm = new ViewManager(this.makeJFrame(false));
        try {
            SecurityController secCtrl = new SecurityController();
            secCtrl.setApplication(this);
            LoginView loginView = secCtrl.loginAction();
            vm.push(loginView);
            
            this.getSecurityManager().addLoginListener((UtilisateurInterface user) -> {
                GestionEcoleController ctrl = new GestionEcoleController();
                ctrl.setApplication(this);
                vm.pop(); // Dispose old window
                vm = new ViewManager(this.makeJFrame());
                vm.push(ctrl.listeClasseAction());
            });
        } catch (Exception e) {
            this.handleException(e);
        }
    }
    
    private void handleException(Exception e) {
        logger.error("Unhandled exception", e);
        
        StringBuilder b = new StringBuilder();
        b.append("Une erreur a eu lieu : \n");
        b.append(e.getMessage() + "\n");
        /*
        for(StackTraceElement s : e.getStackTrace()) {
            b.append(s.toString() + "\n");
        }
        */
        
        JOptionPane.showMessageDialog(
                null,
                b.toString(),
                "Oups...",
                JOptionPane.ERROR_MESSAGE
        );
        
        ViewManager vm = this.getViewManager();
        
        if(vm == null || vm.empty()) {
            this.exit();
        }
    }
    
    /**
     * Exporte la DDL générée par le mapping de Doctrine dans le fichier
     * "db-schema.hibernate5.ddl" du répertoire courant.
     * @return Le nom du fichier dans lequel la DDL a été écrite.
     */
    private String exportDDL() {
        String filename = "db-schema.hibernate5.ddl";
        StandardServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .configure()
                .build();
        Metadata metadata = new MetadataSources(serviceRegistry)
                .buildMetadata();

        new SchemaExport() //
                .setOutputFile(filename)
                .setDelimiter(";")
                .create(EnumSet.of(TargetType.SCRIPT), metadata);

        metadata.buildSessionFactory().close();
        
        return filename;
    }
}

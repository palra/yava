/*
 * Copyright (C) 2017 palra
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xyz.palra.gestion_ecole;

import java.awt.CardLayout;
import java.awt.Container;
import java.awt.event.WindowEvent;
import java.util.Stack;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.apache.log4j.Logger;

/**
 * Gère les vues à afficher sur une JFrame.
 * Les vues sont gérées par une pile de vue, pour avoir un cycle de navigation
 * en profondeur. Lorsqu'on empile une vue (avec la méthode `push`), on garde
 * en mémoire la vue précédente, et on affiche "par dessus" la vue empilée.
 * Lorsqu'on dépile une vue (avec `pop`), on retire la vue du dessus pour afficher
 * celle "d'en dessous".
 * 
 * @author palra
 */
public class ViewManager {
    private JFrame jFrame;
    private Stack<JPanel> panels;
    private static Logger logger = Logger.getLogger(ViewManager.class);

    public ViewManager(JFrame jFrame) {
        this.jFrame = jFrame;
        this.panels = new Stack<>();
    } 
    
    /**
     * Empile une vue sur la JFrame gérée par l'instance.
     * La vue passée en paramètre sera affichée, et les vue "en dessous" retirées
     * de l'affichage, mais gardées en mémoire pour ne pas avoir à les réinstancier.
     * 
     * @param panel La vue à empiler.
     */
    public void push(JPanel panel) {
        this.panels.push(panel);
        this.displayJPanel(panel);
        
        logger.trace("" + this.panels.size() + " panel(s) on the stack");
    }
    
    /**
     * Dépile une vue sur la JFrame gérée par l'instance.
     * La vue actuellement affichée sera déréférencée et supprimée de
     * l'affichage, laissant place à la vue "d'en dessous". Si la pile de vue
     * est vide une fois le déréférencement fait, on dispose la JFrame.
     */
    public void pop() {
        if(this.panels.empty()) {
            logger.debug("Disposing frame");
            this.closeJFrame();
        } else {
            JPanel p = this.panels.pop();
            if(this.panels.empty()) {
                logger.debug("Disposing frame");
                this.closeJFrame();
            } else {
                this.displayJPanel(this.panels.peek());
            }
        }
        
        logger.trace("" + this.panels.size() + " panel(s) on the stack");
    }
    
    /**
     * Ferme la JFrame encapsulée.
     */
    public void closeJFrame() {
        this.jFrame.dispose();
    }
    
    public boolean empty() {
        return this.panels.empty();
    }
    
    private void displayJPanel(JPanel panel) {
        jFrame.getContentPane().removeAll();
        jFrame.getContentPane().add(panel);
        
        jFrame.validate();
        jFrame.repaint();
        jFrame.pack();
        jFrame.setVisible(true);
    }
}

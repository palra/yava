/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package xyz.palra.gestion_ecole.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import xyz.palra.gestion_ecole.model.Eleve;
import xyz.palra.gestion_ecole.view.event.UpdateEleveListener;

/**
 *
 * @author K401LB
 */
public class ShowEleveView extends JPanel {
    
    private Eleve eleve;
    private JTable jTable;
    private JScrollPane jScrollPane;
    private List<UpdateEleveListener> eventListeners;
    private boolean isEditable;
    
    public ShowEleveView(Eleve eleve) {
        this(eleve, false);
    }
      
    public ShowEleveView(Eleve eleve, boolean isEditable) {
        this.eleve = eleve;
        this.eventListeners = new ArrayList<UpdateEleveListener>();
        this.isEditable = isEditable;
        this.make();
    }
    
    
    private void make() {
        // Définition du layout
        this.setLayout(new BorderLayout());
        jTable = new JTable();
        jTable.setModel(new ShowEleveView.ShowEleveModele());
        jTable.setAutoCreateRowSorter(true);
        jTable.setTableHeader(null);
        TableColumn tableColumn = jTable.getColumnModel().getColumn(0);
        tableColumn.setCellRenderer(new CustomCellRenderer());

        jScrollPane = new JScrollPane(jTable);        
        this.add(jScrollPane);
    }
    
    public void addEventListener(UpdateEleveListener eventListener) {
        eventListeners.add(eventListener);
    }
    
    private void notifyEventListeners(Eleve eleve) {
        for (UpdateEleveListener eventListener : eventListeners) {
            eventListener.onUpdateEleve(eleve);
        }
    }
    
    private class ShowEleveModele extends AbstractTableModel {
        private final String[] LABEL = {
            "Nom", "Prenom", "Sexe","Email","Portable(n°)","Domicile(n°)","Ville","CodePostal","NomRue","NumeroRue",
            "EtablissementPrecedent",
            "RemarquesMedicales","MedecinTraitant","MedecinTraitant(n°)"
        };
        
        @Override
        public boolean isCellEditable(int row, int column) {               
            return isEditable && column==1;         
        }

        @Override
        public int getRowCount() {
            return LABEL.length;
        }

        @Override
        public int getColumnCount() {
            return 2;
        }

        @Override
        public Object getValueAt(int row, int col) {
            if(col==1) {
                switch(row) {
                    case 0:
                        return eleve.getIdentite().getNom();
                    case 1:
                        return eleve.getIdentite().getPrenom();
                    case 2:
                        return eleve.getIdentite().getSexe();
                    case 3:
                        return eleve.getIdentite().getEmail();
                    case 4:
                        return eleve.getIdentite().getTelephonePortable();
                    case 5:
                        return eleve.getIdentite().getTelephoneDomicile();
                    case 6:
                        return eleve.getIdentite().getVille();
                    case 7:
                        return eleve.getIdentite().getCodePostal();
                    case 8:
                        return eleve.getIdentite().getNomRue();
                    case 9:
                        return eleve.getIdentite().getNumeroRue();
                    case 10:
                        return eleve.getEtablissementPrecedent();
                    case 11:
                        return eleve.getRemarquesMedicales();
                    case 12:
                        return eleve.getMedecinTraitant();
                    case 13:
                        return eleve.getTelephoneMedecinTraitant();
                    default:
                        return "voir";
                }
            }
            else
                return LABEL[row];
        }
        
        @Override
        public void setValueAt(Object value, int row, int col) {
            if(col == 1) {
                switch(row) {
                    case 0:
                        eleve.getIdentite().setNom((String)value);
                        break;
                    case 1:
                        eleve.getIdentite().setPrenom((String)value);
                        break;
                    case 2:
                        eleve.getIdentite().setSexe((char)value);
                        break;
                    case 3:
                        eleve.getIdentite().setEmail((String)value);
                        break;
                    case 4:
                        eleve.getIdentite().setTelephonePortable((String)value);
                        break;
                    case 5:
                        eleve.getIdentite().setTelephoneDomicile((String)value);
                        break;
                    case 6:
                        eleve.getIdentite().setVille((String)value);
                        break;
                    case 7:
                        eleve.getIdentite().setCodePostal((String)value);
                        break;
                    case 8:
                        eleve.getIdentite().setNomRue((String)value);
                        break;
                    case 9:
                        eleve.getIdentite().setNumeroRue((String)value);
                        break;
                    case 10:
                        eleve.setEtablissementPrecedent((String)value);
                        break;
                    case 11:
                        eleve.setRemarquesMedicales((String)value);
                        break;
                    case 12:
                        eleve.setMedecinTraitant((String)value);
                        break;
                    case 13:
                        eleve.setTelephoneMedecinTraitant((String)value);
                        break;
                }
                fireTableCellUpdated(row, col);
                notifyEventListeners(eleve);
            }
        }
    }
    
    private class CustomCellRenderer extends DefaultTableCellRenderer {
        public Component getTableCellRendererComponent(JTable table, Object value,
          boolean isSelected, boolean hasFocus, int row, int column) {

         Component rendererComp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus,
           row, column);

         //Set foreground color
         rendererComp.setForeground(Color.black);

         //Set background color
         rendererComp .setBackground(Color.gray);
         
         rendererComp.setFocusable(false);
         
         rendererComp.setFont(new Font("Dialog", Font.BOLD, 13));

         return rendererComp ;
        }

    }
}

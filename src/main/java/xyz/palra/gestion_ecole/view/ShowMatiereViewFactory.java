/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.palra.gestion_ecole.view;

import xyz.palra.gestion_ecole.model.Classe;
import xyz.palra.gestion_ecole.model.Matiere;

/**
 *
 * @author palra
 */
@FunctionalInterface
public interface ShowMatiereViewFactory {
    public ShowMatiereView build(Classe classe, Matiere matiere);
}

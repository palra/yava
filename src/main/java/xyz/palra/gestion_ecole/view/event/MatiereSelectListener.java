/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.palra.gestion_ecole.view.event;

import java.util.EventListener;
import xyz.palra.gestion_ecole.model.Eleve;
import xyz.palra.gestion_ecole.model.Matiere;

/**
 * Interface de listener pour la sélection d'une matière dans la GUI.
 * @author alexis
 */
@FunctionalInterface
public interface MatiereSelectListener extends EventListener {
    public abstract void onMatiereSelect(Matiere matiere);
}

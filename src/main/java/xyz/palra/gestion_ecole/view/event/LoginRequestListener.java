/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.palra.gestion_ecole.view.event;

import java.util.EventListener;

/**
 * Interface de listener pour la validation du formulaire de login
 * @author palra
 */
@FunctionalInterface
public interface LoginRequestListener extends EventListener {
    public abstract void onLoginRequest(String email, String password);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.palra.gestion_ecole.view.event;

import java.util.EventListener;
import xyz.palra.gestion_ecole.model.Eleve;

/**
 *
 * @author K401LB
 */

@FunctionalInterface
public interface UpdateEleveListener extends EventListener {
    public void onUpdateEleve(Eleve eleve);
}

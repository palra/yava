/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.palra.gestion_ecole.view.event;

import java.util.EventListener;
import java.util.Map;
import xyz.palra.gestion_ecole.model.Eleve;

/**
 *
 * @author alexis
 */
@FunctionalInterface
public interface EvalCreateListener extends EventListener {
    public abstract void onEvalCreate(Map<Eleve,String> notes);
}

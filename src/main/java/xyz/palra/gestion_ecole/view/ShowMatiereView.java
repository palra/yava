/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.palra.gestion_ecole.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.util.List;
import java.util.Map;
import javax.swing.SwingConstants;
import javax.swing.table.AbstractTableModel;
import xyz.palra.gestion_ecole.model.Classe;
import xyz.palra.gestion_ecole.model.Eleve;
import xyz.palra.gestion_ecole.model.Evaluation;
import xyz.palra.gestion_ecole.model.Matiere;
import xyz.palra.gestion_ecole.view.ListeClasseView;
import xyz.palra.gestion_ecole.view.event.EvalCreateListener;

/**
 *
 * @author alexis
 */
public class ShowMatiereView extends JPanel{
    
    private Classe classe;
    private Matiere matiere;
    private Map<Eleve,String> notes;
    private JTable jTable;
    private JScrollPane jScrollPane;
    private JButton jButtonValidate;
    private boolean isEditable;
    
    public ShowMatiereView(Classe classe, Matiere matiere) {
        this(classe, matiere, false);
    }
    
    public ShowMatiereView(Classe classe, Matiere matiere, boolean isEditable) {
        this.matiere = matiere;
        this.classe = classe;
        this.isEditable = isEditable;
        this.initNotes();
        this.make();
    }
    
    private void make() {
        // Définition du layout
        this.setLayout(new BorderLayout());
        jTable = new JTable();
        jTable.setModel(new ShowMatiereView.ShowMatiereModele());
        jTable.setAutoCreateRowSorter(true);
        
        // Titre de page
        JLabel titre = new JLabel(matiere.getNom(), SwingConstants.CENTER);
        this.add(titre, BorderLayout.PAGE_START);
        
        jScrollPane = new JScrollPane(jTable);
        this.add(jScrollPane);
        
        // Boutons en bas de page
        JPanel jBottomButtons = new JPanel();
        jButtonValidate = new JButton("Valider");
        
        jBottomButtons.add(jButtonValidate);
        this.add(jBottomButtons, BorderLayout.PAGE_END);
    }
    
    public void addEvalCreateListener(EvalCreateListener listener) {
       jButtonValidate.addActionListener((ActionEvent ae) -> {
           listener.onEvalCreate(notes);
       });
    }
    
    public void initNotes() {
        this.notes = new HashMap<Eleve,String>();
        for(Eleve e : classe.getEleves()) {
            notes.put(e,"");
        }
    }
    
    private class ShowMatiereModele extends AbstractTableModel {
        private final String[] LABEL = {"Nom", "Note/20"};
        
        @Override
        public boolean isCellEditable(int row, int column) {               
            return isEditable && column==1;         
        }
        
        @Override
        public int getRowCount() {
            return classe.getEleves().size();
        }

        @Override
        public int getColumnCount() {
            return LABEL.length;
        }

        @Override
        public String getColumnName(int i) {
            return LABEL[i];
        }

        @Override
        public Object getValueAt(int row, int col) {
            Eleve e = classe.getEleves().get(row);
            switch(col) {
                case 0:
                    return e.getIdentite().getNom() + " " + e.getIdentite().getPrenom();
                case 1:
                    return notes.get(e);
                default:
                    return "voir";
            }
        }
        
        @Override
        public void setValueAt(Object value, int row, int col) {
            if(col == 1) {
                Eleve e = classe.getEleves().get(row);
                notes.put(e,(String) value);
                fireTableCellUpdated(row, col);
            }
        }
    }
}

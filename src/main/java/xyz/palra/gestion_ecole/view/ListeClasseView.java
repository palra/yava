/*
 * Copyright (C) 2017 palra
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xyz.palra.gestion_ecole.view;

import java.awt.BorderLayout;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.AbstractTableModel;
import xyz.palra.gestion_ecole.model.Classe;
import xyz.palra.gestion_ecole.view.event.ClassSelectListener;

/**
 * Vue de la liste des classes de l'école
 * @author palra
 */
public class ListeClasseView extends JPanel {

    private List<Classe> classes;
    private JTable jTable;
    private JScrollPane jScrollPane;
    
    public ListeClasseView(List<Classe> classes) {
        this.classes = classes;   
        this.make();
    }   
    
    private void make() {
        // Définition du layout
        this.setLayout(new BorderLayout());
        
        // Titre
        JLabel titre = new JLabel("Sélection de classe", SwingConstants.CENTER);
        this.add(titre, BorderLayout.PAGE_START);
        
        // Table centrale
        jTable = new JTable();
        jTable.setModel(new ListeClassesModele());
        jTable.setAutoCreateRowSorter(true);

        jScrollPane = new JScrollPane(jTable);
        
        this.add(new JLabel("BIENVENUE",SwingConstants.CENTER),BorderLayout.PAGE_START);
        this.add(jScrollPane,BorderLayout.CENTER);
    }
    
    public void addClassSelectListener(ClassSelectListener listener) {
        jTable.getSelectionModel().addListSelectionListener((ListSelectionEvent lse) -> {
            if(jTable.getSelectedRow() > -1) {
                listener.onClassSelect(classes.get(jTable.getSelectedRow()));
            }
            
            jTable.getSelectionModel().clearSelection();
        });
    }
    
    private class ListeClassesModele extends AbstractTableModel {
        private final String[] LABEL = {"Nom", "Année promotion"};
        
        @Override
        public int getRowCount() {
            return classes.size();
        }

        @Override
        public int getColumnCount() {
            return LABEL.length;
        }
        
        @Override
        public String getColumnName(int col) {
            return LABEL[col];
        }

        @Override
        public Object getValueAt(int row, int col) {
            Classe c = classes.get(row);
            switch(col) {
                case 0:
                    return c.getNom();
                default:
                    return c.getAnneePromotion();
            }
        }
    }
}

/*
 * Copyright (C) 2017 palra
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xyz.palra.gestion_ecole.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.List;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.AbstractTableModel;
import xyz.palra.gestion_ecole.model.Eleve;
import xyz.palra.gestion_ecole.view.event.EleveSelectListener;

/**
 *
 * @author palra
 */
public class ListeEleveView extends JPanel {
    private List<Eleve> eleves;
    private JTable jTable;
    private JSplitPane jSplitPane;
    private ShowEleveViewFactory viewFactory;
    
    public ListeEleveView(List<Eleve> eleves) {
        this.eleves = eleves;
        this.make();
    }
    
    private void make() {
        // Définition du layout
        this.setLayout(new BorderLayout());
        
        // Left pane : table
        JPanel leftPanel = new JPanel();
        jTable = new JTable();
        jTable.setModel(new ListeEleveModele());
        jTable.setAutoCreateRowSorter(true);
        JScrollPane jScrollPane = new JScrollPane(jTable);
        
        // Right pane : show eleve (TODO)
        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.X_AXIS));
        rightPanel.add(Box.createHorizontalGlue());
        rightPanel.add(new JLabel("Sélectionnez un élève"));
        rightPanel.add(Box.createHorizontalGlue());
        
        // Split Pane
        jSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        jSplitPane.setLeftComponent(jScrollPane);
        jSplitPane.setRightComponent(rightPanel);
        jSplitPane.setOneTouchExpandable(true);
        jSplitPane.setDividerLocation(300);
        jSplitPane.setResizeWeight(0.5);
        this.setupEleveSelectListener();
        
        this.add(jSplitPane);
    }
    
    public void addEleveSelectListener(EleveSelectListener listener) {
        jTable.getSelectionModel().addListSelectionListener((ListSelectionEvent lse) -> {
            if(jTable.getSelectedRow() > -1) {
                listener.onEleveSelect(eleves.get(jTable.getSelectedRow()));
            }
            
            jTable.getSelectionModel().clearSelection();
        });
    }

    public void setViewFactory(ShowEleveViewFactory factory) {
        this.viewFactory = factory;
    }
    
    private void setupEleveSelectListener() {
        this.addEleveSelectListener((Eleve eleve) -> {
            JPanel jPanel = new JPanel();
            jPanel.setLayout(new BoxLayout(jPanel, BoxLayout.X_AXIS));
            jPanel.add(Box.createHorizontalGlue());
            jPanel.add(viewFactory.build(eleve));
            jPanel.add(Box.createHorizontalGlue());
        
            jSplitPane.setRightComponent(jPanel);
            jSplitPane.resetToPreferredSizes();
        });
    }
    
    private class ListeEleveModele extends AbstractTableModel {
        private final String[] LABEL = {"Nom", "Moyenne", "Actions"};

        @Override
        public int getRowCount() {
            return eleves.size();
        }

        @Override
        public int getColumnCount() {
            return LABEL.length;
        }

        @Override
        public String getColumnName(int i) {
            return LABEL[i];
        }

        @Override
        public Object getValueAt(int row, int col) {
            Eleve e = eleves.get(row);
            switch(col) {
                case 0:
                    return e.getIdentite().getNom() + " " + e.getIdentite().getPrenom();
                case 1:
                    return e.getMoyenne();
                default:
                    return "voir";
            }
        }
    }
}

/*
 * Copyright (C) 2017 palra
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xyz.palra.gestion_ecole.view;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import xyz.palra.gestion_ecole.model.Classe;
import xyz.palra.gestion_ecole.view.ListeEleveView;

/**
 *
 * @author palra
 */
public class ShowClasseView extends JPanel {
    private Classe classe;
    private JButton jButtonExit;
    private JTabbedPane jTabbedPane;
    private JPanel jEleveTab = new JPanel();
    private JPanel jMatiereTab = new JPanel();

    public ShowClasseView(Classe classe) {
        this.classe = classe;
        this.make();
    }
    
    private void make() {
        // Définition du layout
        this.setLayout(new BorderLayout());
        
        // Titre de page
        JLabel titre = new JLabel(classe.getNom(), SwingConstants.CENTER);
        this.add(titre, BorderLayout.PAGE_START);
        
        jTabbedPane = new JTabbedPane();
        
        jTabbedPane.addTab("Élèves", jEleveTab);
        jTabbedPane.addTab("Matières", jMatiereTab);
                
        this.add(jTabbedPane, BorderLayout.CENTER);
        
        // Boutons en bas de page
        JPanel jBottomButtons = new JPanel();
        jButtonExit = new JButton("Retour");
        jBottomButtons.add(jButtonExit);
        this.add(jBottomButtons, BorderLayout.PAGE_END);
    }
    
    public void setListeEleveView(ListeEleveView view) {
        jEleveTab.removeAll();
        jEleveTab.add(view);
    }
    
    public void setListeMatieresView(ListeMatiereView view) {
        jMatiereTab.removeAll();
        jMatiereTab.add(view);
    }
    
    
    public void addReturnListener(ActionListener listener) {
        jButtonExit.addActionListener(listener);
    }
}

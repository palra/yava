/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.palra.gestion_ecole.view;

import java.awt.BorderLayout;
import java.util.List;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.AbstractTableModel;
import xyz.palra.gestion_ecole.model.Classe;
import xyz.palra.gestion_ecole.model.Eleve;
import xyz.palra.gestion_ecole.model.Matiere;
import xyz.palra.gestion_ecole.view.event.EleveSelectListener;
import xyz.palra.gestion_ecole.view.event.MatiereSelectListener;

/**
 *
 * @author palra
 */
public class ListeMatiereView extends JPanel {
    private Classe classe;
    private List<Matiere> matieres;
    private JTable jTable;
    private JSplitPane jSplitPane;
    private ShowMatiereViewFactory viewFactory;

    public ListeMatiereView(Classe classe, List<Matiere> matieres) {
        this.classe = classe;
        this.matieres = matieres;
        this.make();      
    }

    public void setViewFactory(ShowMatiereViewFactory viewFactory) {
        this.viewFactory = viewFactory;
    }
    
    private void make() {
        // Définition du layout
        this.setLayout(new BorderLayout());
        
        // Left pane : table
        jTable = new JTable();
        jTable.setModel(new ListeMatiereModele());
        jTable.setAutoCreateRowSorter(true);
        JScrollPane jScrollPane = new JScrollPane(jTable);
        
        // Right pane : show eleve 
        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.X_AXIS));
        rightPanel.add(Box.createHorizontalGlue());
        rightPanel.add(new JLabel("Sélectionnez une matière"));
        rightPanel.add(Box.createHorizontalGlue());
        
        // Split Pane
        jSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        jSplitPane.setLeftComponent(jScrollPane);
        jSplitPane.setRightComponent(rightPanel);
        jSplitPane.setOneTouchExpandable(true);
        jSplitPane.setDividerLocation(300);
        jSplitPane.setResizeWeight(0.5);
        this.setupMatiereSelectListener();
        
        
        this.add(jSplitPane);
    }
    
    public void addMatiereSelectListener(MatiereSelectListener listener) {
        jTable.getSelectionModel().addListSelectionListener((ListSelectionEvent lse) -> {
            if(jTable.getSelectedRow() > -1) {
                listener.onMatiereSelect(matieres.get(jTable.getSelectedRow()));
            }
            
            jTable.getSelectionModel().clearSelection();
        });
    }
    
    private void setupMatiereSelectListener() {
        this.addMatiereSelectListener((Matiere matiere) -> {
            JPanel jPanel = new JPanel();
            jPanel.setLayout(new BoxLayout(jPanel, BoxLayout.X_AXIS));
            jPanel.add(Box.createHorizontalGlue());
            jPanel.add(viewFactory.build(classe, matiere));
            jPanel.add(Box.createHorizontalGlue());
        
            jSplitPane.setRightComponent(jPanel);
            jSplitPane.resetToPreferredSizes();
        });
    }
    
    private class ListeMatiereModele extends AbstractTableModel {
        private final String[] LABEL = {"Nom", "Action"};

        @Override
        public int getRowCount() {
            return matieres.size();
        }

        @Override
        public int getColumnCount() {
            return LABEL.length;
        }
        
        @Override
        public String getColumnName(int i) {
            return LABEL[i];
        }

        @Override
        public Object getValueAt(int row, int col) {
            Matiere m = matieres.get(row);
            switch(col) {
                case 0:
                    return m.getNom();
                default:
                    return "Evaluation";
            }
        }   
    }
}

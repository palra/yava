/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.palra.gestion_ecole.view;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import xyz.palra.gestion_ecole.view.event.LoginRequestListener;

/**
 * 
 * @author palra
 */
public class LoginView extends JPanel {
     
    private JTextField jEmailField;  
    private JPasswordField jPassField;
    private JButton jLoginBtn  = new JButton("Connexion");

    public LoginView() {
        this.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        
        JLabel jEmailLabel = new JLabel("Email");
        jEmailField = new JTextField();
        jEmailField.setMinimumSize(jEmailField.getPreferredSize());
        
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.weightx = 1;
        c.gridwidth = 1;
        c.gridy = 0;
        this.add(jEmailLabel, c);
        
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 2;
        c.weightx = 3;
        c.gridwidth = 2;
        c.gridy = 0;
        this.add(jEmailField, c);
        
        JLabel jPassLabel  = new JLabel("Mot de passe"); 
        jPassField = new JPasswordField();
        jPassField.setMinimumSize(jPassField.getPreferredSize());
        
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.weightx = 1;
        c.gridwidth = 1;
        c.gridy = 1;
        this.add(jPassLabel, c);
        
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 2;
        c.weightx = 3;
        c.gridwidth = 2;
        c.gridy = 1;
        this.add(jPassField, c);
        
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.weightx = 1;
        c.gridwidth = 3;
        c.gridy = 2;
        c.insets = new Insets(10, 0, 0, 0);
        this.add(jLoginBtn, c);
        
        this.setBorder(new EmptyBorder(20, 20, 20, 20));
        this.setPreferredSize(new Dimension(400, 150));
    }
    
    public void addLoginListener(LoginRequestListener listener) {
        jLoginBtn.addActionListener((ActionEvent ae) -> {
            listener.onLoginRequest(
                    jEmailField.getText(),
                    jPassField.getText()
            );
        });
    }
    
    public void notifyLoginError() {
        JOptionPane.showMessageDialog(this, "Email/mot de passe invalide", "Oups...", JOptionPane.ERROR_MESSAGE);
    }
}

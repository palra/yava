/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.palra.gestion_ecole.view;

import xyz.palra.gestion_ecole.model.Eleve;

/**
 *
 * @author palra
 */
@FunctionalInterface
public interface ShowEleveViewFactory {
    public ShowEleveView build(Eleve eleve);
}

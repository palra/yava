/*
 * Copyright (C) 2017 palra
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xyz.palra.gestion_ecole.model;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Représente l'identité d'une personne physique
 * @author palra
 */
@Embeddable
public class Identite {
    public static final char SEXE_HOMME = 'h';
    public static final char SEXE_FEMME = 'f';
    
    @Column(name = "nom")
    private String nom;    
    @Column(name = "prenom")
    private String prenom;    
    @Column(name = "sexe")
    private char sexe;    
    @Column(name = "numero_rue")
    private String numeroRue; // String pour gérer le cas des `2bis`    
    @Column(name = "nom_rue")
    private String nomRue;    
    @Column(name = "code_postal")
    private String codePostal;   
    @Column(name = "ville")
    private String ville;    
    @Column(name = "telephone_portable")
    private String telephonePortable;    
    @Column(name = "telephone_domicile")
    private String telephoneDomicile;    
    @Column(name = "email")
    private String email;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public char getSexe() {
        return sexe;
    }

    public void setSexe(char sexe) {
        this.sexe = sexe;
    }

    public String getNumeroRue() {
        return numeroRue;
    }

    public void setNumeroRue(String numeroRue) {
        this.numeroRue = numeroRue;
    }

    public String getNomRue() {
        return nomRue;
    }

    public void setNomRue(String nomRue) {
        this.nomRue = nomRue;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getTelephonePortable() {
        return telephonePortable;
    }

    public void setTelephonePortable(String telephonePortable) {
        this.telephonePortable = telephonePortable;
    }

    public String getTelephoneDomicile() {
        return telephoneDomicile;
    }

    public void setTelephoneDomicile(String telephoneDomicile) {
        this.telephoneDomicile = telephoneDomicile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Identite other = (Identite) obj;
        if (this.sexe != other.sexe) {
            return false;
        }
        if (!Objects.equals(this.nom, other.nom)) {
            return false;
        }
        if (!Objects.equals(this.prenom, other.prenom)) {
            return false;
        }
        if (!Objects.equals(this.numeroRue, other.numeroRue)) {
            return false;
        }
        if (!Objects.equals(this.nomRue, other.nomRue)) {
            return false;
        }
        if (!Objects.equals(this.codePostal, other.codePostal)) {
            return false;
        }
        if (!Objects.equals(this.ville, other.ville)) {
            return false;
        }
        if (!Objects.equals(this.telephonePortable, other.telephonePortable)) {
            return false;
        }
        if (!Objects.equals(this.telephoneDomicile, other.telephoneDomicile)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.nom);
        hash = 23 * hash + Objects.hashCode(this.prenom);
        hash = 23 * hash + this.sexe;
        hash = 23 * hash + Objects.hashCode(this.numeroRue);
        hash = 23 * hash + Objects.hashCode(this.nomRue);
        hash = 23 * hash + Objects.hashCode(this.codePostal);
        hash = 23 * hash + Objects.hashCode(this.ville);
        hash = 23 * hash + Objects.hashCode(this.telephonePortable);
        hash = 23 * hash + Objects.hashCode(this.telephoneDomicile);
        hash = 23 * hash + Objects.hashCode(this.email);
        return hash;
    }
}

/*
 * Copyright (C) 2017 palra
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xyz.palra.gestion_ecole.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 * Représente un élève de l'école
 * @author palra
 */
@Entity
@Table( name = "eleve" )
public class Eleve {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @Embedded
    private Identite identite;
    
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "eleve")
    private List<Responsable> responsables;
    
    private Date dateInscription;
    private String etablissementPrecedent;
    private String medecinTraitant;
    private String telephoneMedecinTraitant;
    private String allergies;
    private String remarquesMedicales;
    
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "eleve")
    private Set<Evaluation> evaluations;
    
    @ManyToOne
    @JoinColumn(nullable = false)
    private Classe classe;

    public Eleve() {
        this.responsables = new ArrayList<>();
        this.evaluations = new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Identite getIdentite() {
        return identite;
    }

    public void setIdentite(Identite identite) {
        this.identite = identite;
    }

    public List<Responsable> getResponsables() {
        return responsables;
    }

    public void addResponsable(Responsable responsable) {
        if(!hasResponsable(responsable)) {
            this.responsables.add(responsable);
        }
        
        if(this != responsable.getEleve()) {
            responsable.setEleve(this);
        }
    }
    
    public boolean hasResponsable(Responsable responsable) {
        return this.responsables.contains(responsable);
    }

    public Date getDateInscription() {
        return dateInscription;
    }

    public void setDateInscription(Date dateInscription) {
        this.dateInscription = dateInscription;
    }

    public String getEtablissementPrecedent() {
        return etablissementPrecedent;
    }

    public void setEtablissementPrecedent(String etablissementPrecedent) {
        this.etablissementPrecedent = etablissementPrecedent;
    }

    public String getMedecinTraitant() {
        return medecinTraitant;
    }

    public void setMedecinTraitant(String medecinTraitant) {
        this.medecinTraitant = medecinTraitant;
    }

    public String getTelephoneMedecinTraitant() {
        return telephoneMedecinTraitant;
    }

    public void setTelephoneMedecinTraitant(String telephoneMedecinTraitant) {
        this.telephoneMedecinTraitant = telephoneMedecinTraitant;
    }

    public String getAllergies() {
        return allergies;
    }

    public void setAllergies(String allergies) {
        this.allergies = allergies;
    }

    public String getRemarquesMedicales() {
        return remarquesMedicales;
    }

    public void setRemarquesMedicales(String remarquesMedicales) {
        this.remarquesMedicales = remarquesMedicales;
    }

    public Set<Evaluation> getEvaluations() {
        return evaluations;
    }
    
    public void addEvaluation(Evaluation evaluation) {
        if(!hasEvaluation(evaluation)) {
            this.evaluations.add(evaluation);
        }
        
        if(this != evaluation.getEleve()) {
            evaluation.setEleve(this);
        }
    }
    
    public void removeEvaluation(Evaluation evaluation) {
        this.evaluations.remove(evaluation);
    }
    
    public boolean hasEvaluation(Evaluation evaluation) {
        return this.evaluations.contains(evaluation);
    }

    public Classe getClasse() {
        return classe;
    }

    public void setClasse(Classe classe) {
        this.classe = classe;
        classe.addEleve(this);
    }
    
    public float getMoyenne() {
        float moyenne = 0;
        
        for(Evaluation eval: evaluations) {
            moyenne += eval.getNote();
        }
        
        return evaluations.isEmpty() ? 0 : moyenne / evaluations.size();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Eleve other = (Eleve) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.etablissementPrecedent, other.etablissementPrecedent)) {
            return false;
        }
        if (!Objects.equals(this.medecinTraitant, other.medecinTraitant)) {
            return false;
        }
        if (!Objects.equals(this.telephoneMedecinTraitant, other.telephoneMedecinTraitant)) {
            return false;
        }
        if (!Objects.equals(this.allergies, other.allergies)) {
            return false;
        }
        if (!Objects.equals(this.remarquesMedicales, other.remarquesMedicales)) {
            return false;
        }
        if (!Objects.equals(this.identite, other.identite)) {
            return false;
        }
        if (!Objects.equals(this.responsables, other.responsables)) {
            return false;
        }
        if (!Objects.equals(this.dateInscription, other.dateInscription)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + this.id;
        hash = 53 * hash + Objects.hashCode(this.identite);
        hash = 53 * hash + Objects.hashCode(this.responsables);
        hash = 53 * hash + Objects.hashCode(this.dateInscription);
        hash = 53 * hash + Objects.hashCode(this.etablissementPrecedent);
        hash = 53 * hash + Objects.hashCode(this.medecinTraitant);
        hash = 53 * hash + Objects.hashCode(this.telephoneMedecinTraitant);
        hash = 53 * hash + Objects.hashCode(this.allergies);
        hash = 53 * hash + Objects.hashCode(this.remarquesMedicales);
        return hash;
    }
}

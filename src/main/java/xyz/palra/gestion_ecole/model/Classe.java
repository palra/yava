/*
 * Copyright (C) 2017 palra
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xyz.palra.gestion_ecole.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Observable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * Représente une classe d'élèves
 * @author palra
 */
@Entity
@Table( name = "classe" )
public class Classe {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nom;
    private int anneePromotion;
    
    @ManyToMany(cascade = CascadeType.ALL)
    private List<Matiere> matieres;
    
    @OneToMany(cascade = CascadeType.PERSIST, orphanRemoval = true, mappedBy = "classe")
    private List<Eleve> eleves;

    public Classe() {
        this.matieres = new ArrayList<Matiere>();
        this.eleves = new ArrayList<Eleve>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getAnneePromotion() {
        return anneePromotion;
    }

    public void setAnneePromotion(int annéePromotion) {
        this.anneePromotion = annéePromotion;
    }

    public List<Matiere> getMatieres() {
        return matieres;
    }
    
    public void addMatiere(Matiere matiere) {
        this.matieres.add(matiere);
    }
    
    public void removeMatiere(Matiere matiere) {
        this.matieres.remove(matiere);
    }

    public List<Eleve> getEleves() {
        return eleves;
    }
    
    public boolean hasEleve(Eleve e) {
        return this.eleves.contains(e);
    }
    
    public void addEleve(Eleve e) {
        if(!this.hasEleve(e)) {
            this.eleves.add(e);
        }
        
        if(e.getClasse() != this) {
            e.setClasse(this);
        }
    }
    
    public void removeEleve(Eleve e) {
        if(this.hasEleve(e)) {
            this.eleves.remove(e);
        }
        
        if(e.getClasse() == this) {
            e.setClasse(null);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Classe other = (Classe) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.anneePromotion != other.anneePromotion) {
            return false;
        }
        if (!Objects.equals(this.nom, other.nom)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + this.id;
        hash = 47 * hash + Objects.hashCode(this.nom);
        hash = 47 * hash + this.anneePromotion;
        return hash;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.palra.gestion_ecole.security;

import java.util.List;

/**
 * Représente le contrat minimal d'une classe utilisateur.
 * @author palra
 */
public interface UtilisateurInterface {
    public String getEmail();
    public String getPassword();
    public List<String> getRoles();
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.palra.gestion_ecole.security;

/**
 * Représente le contrat d'un repository d'utilisateurs utilisé par le contexte
 * de sécurité.
 * @author palra
 */
public interface UtilisateurRepositoryInterface {
    /**
     * Retrouve un utilisateur par son email et son mot de passe.
     * @param email L'email de l'utilisateur
     * @param password Le mot de passe de l'utilisateur
     * @return L'instance de l'utilisateur si il a été trouvé, null sinon.
     */
    public UtilisateurInterface findByEmailPassword(String email, String password);
}

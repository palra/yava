/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.palra.gestion_ecole.security;

import java.util.ArrayList;
import java.util.List;
import xyz.palra.gestion_ecole.security.event.LoginListener;

/**
 * Gestionaire de la sécurité dans l'application.
 * @author palra
 */
public class SecurityManager {
    private UtilisateurInterface currentUser;
    private UtilisateurRepositoryInterface userRepo;
    private List<LoginListener> loginLiseners;

    public SecurityManager(UtilisateurRepositoryInterface userRepo) {
        this.userRepo = userRepo;
        this.loginLiseners = new ArrayList<>();
    }
    
    /**
     * @return l'utilisateur en cours, null si il n'est pas authentifié.
     */
    public UtilisateurInterface getCurrentUser() {
        return this.currentUser;
    }
    
    /**
     * @return true si un utilisateur est authentifié, false sinon
     */
    public boolean isLoggedIn() {
        return this.currentUser != null;
    }
    
    /**
     * Authentifie un utilisateur donné.
     * 
     * @param email L'email de l'utilisateur
     * @param password Le mot de passe de l'utilisateur
     * @return true si l'authentification a eu lieu avec succès, false sinon.
     */
    public boolean login(String email, String password) {
        this.currentUser = this.userRepo.findByEmailPassword(email, password);

        // Notification Login
        if(this.isLoggedIn()) {
            for(LoginListener l : this.loginLiseners) {
                l.onLogin(this.getCurrentUser());
            }
        }
        
        return this.isLoggedIn();
    }
    
    /**
     * Détermine si l'utilisateur courant possède le rôle spécifié.
     * 
     * @param role Le rôle à tester
     * @return true si le rôle spécifié est attribué à l'utilisateur courant,
     * false sinon
     * @throws NonAuthentifieException si l'utilisateur n'est pas authentifié
     */
    public boolean isGranted(String role) throws NonAuthentifieException {
        if(!this.isLoggedIn()) {
            throw new NonAuthentifieException();
        }
        
        return this.currentUser.getRoles().contains(role);
    }
    
    public void addLoginListener(LoginListener listener) {
        this.loginLiseners.add(listener);
    }
}

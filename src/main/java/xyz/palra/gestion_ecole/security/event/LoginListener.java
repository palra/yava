/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.palra.gestion_ecole.security.event;

import java.util.EventListener;
import xyz.palra.gestion_ecole.security.UtilisateurInterface;

/**
 *
 * @author palra
 */
@FunctionalInterface
public interface LoginListener extends EventListener {
    public abstract void onLogin(UtilisateurInterface user);
}

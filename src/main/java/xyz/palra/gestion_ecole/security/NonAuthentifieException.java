/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.palra.gestion_ecole.security;

/**
 *
 * @author palra
 */
public class NonAuthentifieException extends Exception {
    public NonAuthentifieException() {
        super("L'utilisateur n'est pas authentifié.");
    }
}


/*
 * Copyright (C) 2017 palra
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xyz.palra.gestion_ecole.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import xyz.palra.gestion_ecole.model.Eleve;
import xyz.palra.gestion_ecole.model.Identite;


/**
 *
 * @author palra
 */
public abstract class EleveMockData {
    public static List<Eleve> getListEleves() {
        List<Eleve> list = new ArrayList<Eleve>();
        
        Eleve e = new Eleve();
            Identite i = new Identite();
            i.setNom("Dupont");
            i.setPrenom("Michel");
            i.setSexe(Identite.SEXE_HOMME);
            e.setIdentite(i);
        e.setDateInscription(new Date(1504483200000L));
        list.add(e);
        
        return list;
    }
    
    public static Eleve getEleve() {
        Eleve e = new Eleve();
            Identite i = new Identite();
            i.setNom("Dupont");
            i.setPrenom("Michel");
            i.setSexe(Identite.SEXE_HOMME);
            e.setIdentite(i);
        e.setClasse(ClasseMockData.getClasse());
        e.setDateInscription(new Date(1504483200000L));
        
        return e;
    }
}

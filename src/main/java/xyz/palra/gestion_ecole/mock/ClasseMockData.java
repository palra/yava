/*
 * Copyright (C) 2017 palra
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xyz.palra.gestion_ecole.mock;

import java.util.ArrayList;
import java.util.List;
import xyz.palra.gestion_ecole.model.Classe;

/**
 *
 * @author palra
 */
public abstract class ClasseMockData {
    public static List<Classe> getListClasses() {
        List<Classe> list = new ArrayList<Classe>();
        
        Classe classe = new Classe();
        classe.setNom("4ème 1");
        classe.setAnneePromotion(2017);
        list.add(classe);
        
        classe = new Classe();
        classe.setNom("4ème 2");
        classe.setAnneePromotion(2017);
        list.add(classe);
        
        classe = new Classe();
        classe.setNom("4ème 3");
        classe.setAnneePromotion(2017);
        list.add(classe);
        
        classe = new Classe();
        classe.setNom("4ème 4");
        classe.setAnneePromotion(2017);
        list.add(classe);
        
        classe = new Classe();
        classe.setNom("4ème 5");
        classe.setAnneePromotion(2017);
        list.add(classe);
        
        return list;
    }
    
    public static Classe getClasse() {
        Classe classe = new Classe();
        classe.setNom("4ème 1");
        classe.setAnneePromotion(2017);
        
        classe.getEleves().addAll(EleveMockData.getListEleves());
        classe.getMatieres().addAll(MatiereMockData.getListMatieres());
        
        return classe;
    }
}

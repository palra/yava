/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.palra.gestion_ecole.mock;

import java.util.ArrayList;
import java.util.List;
import xyz.palra.gestion_ecole.model.Matiere;

/**
 *
 * @author palra
 */
public abstract class MatiereMockData {
    public static List<Matiere> getListMatieres() {
        return new ArrayList<Matiere>(){{
            add(new Matiere(){{ setNom("Français"); }});
            add(new Matiere(){{ setNom("Mathématiques"); }});
            add(new Matiere(){{ setNom("Anglais"); }});
            add(new Matiere(){{ setNom("Espagnol"); }});
            add(new Matiere(){{ setNom("SVT"); }});
            add(new Matiere(){{ setNom("Physique-Chimie"); }});
        }};
    }
}

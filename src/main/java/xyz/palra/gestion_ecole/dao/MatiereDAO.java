/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.palra.gestion_ecole.dao;

import java.util.List;
import org.hibernate.Session;
import xyz.palra.gestion_ecole.mock.EleveMockData;
import xyz.palra.gestion_ecole.model.Classe;
import xyz.palra.gestion_ecole.model.Eleve;
import xyz.palra.gestion_ecole.model.Matiere;

/**
 *
 * @author alexis
 */
/**
 * DAO de gestion des matieres.
 */
public class MatiereDAO extends AbstractDAO<Matiere> {

    public MatiereDAO(Session session) {
        super(session);
    }

    @Override
    public List<Matiere> findAll() {
        List<Matiere> result;
        Session session = this.getCurrentSession();
        session.beginTransaction();
        result = session.createQuery( "SELECT m FROM Matiere m", Matiere.class ).list();
        session.getTransaction().commit();
        
        return result;
    }
    
    @Override
    public Matiere findById(int id) {
        Session session = this.getCurrentSession();
        session.beginTransaction();
        Matiere matiere = (Matiere) session.get(Matiere.class,id);
        session.getTransaction().commit();
        
        return matiere;
    }

    @Override
    public Matiere create(Matiere obj) {
        Session session = this.getCurrentSession();
        session.beginTransaction();
        session.save(obj);
        session.getTransaction().commit();
                
        return obj;
    }

    @Override
    public Matiere update(Matiere obj) {
        Session session = this.getCurrentSession();
        session.beginTransaction();
        session.update(obj);
        session.getTransaction().commit();      
        
        return obj;
    }

    @Override
    public void delete(Matiere obj) {
        Session session = this.getCurrentSession();
        session.beginTransaction();
        session.delete(obj);
        session.getTransaction().commit();
    }

}

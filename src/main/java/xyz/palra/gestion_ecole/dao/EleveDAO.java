/*
 * Copyright (C) 2017 palra
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xyz.palra.gestion_ecole.dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;
import xyz.palra.gestion_ecole.model.Eleve;
import xyz.palra.gestion_ecole.model.Identite;
import xyz.palra.gestion_ecole.mock.EleveMockData;
import xyz.palra.gestion_ecole.model.Evaluation;
import xyz.palra.gestion_ecole.model.Matiere;

/**
 *
 * @author palra
 */
public class EleveDAO extends AbstractDAO<Eleve> {

    public EleveDAO(Session session) {
        super(session);
    }

    @Override
    public List<Eleve> findAll() {
        List<Eleve> result;
        Session session = this.getCurrentSession();
        session.beginTransaction();
        result = session.createQuery( "SELECT e FROM Eleve e", Eleve.class ).list();
        session.getTransaction().commit();
        
        return result;
    }

    @Override
    public Eleve findById(int id) {
        Session session = this.getCurrentSession();
        session.beginTransaction();
        Eleve eleve = (Eleve) session.get(Eleve.class,id);
        session.getTransaction().commit();
        
        return eleve;
    }
    
    public List<Evaluation> findEvaluations(Eleve obj) {
        Session session = this.getCurrentSession();
        session.beginTransaction();
        List<Evaluation> result = session.createQuery( "SELECT e FROM Evaluation e where e.eleve=:eleve" ).setParameter("eleve", obj.getId()).list();
        session.getTransaction().commit();
        
        return result;
    }

    @Override
    public Eleve create(Eleve obj) {
        Session session = this.getCurrentSession();
        session.beginTransaction();
        session.save(obj);
        session.getTransaction().commit();
                
        return obj;
    }

    @Override
    public Eleve update(Eleve obj) {
        Session session = this.getCurrentSession();
        session.beginTransaction();
        session.update(obj);
        session.getTransaction().commit();      
        
        return obj;
    }

    @Override
    public void delete(Eleve obj) {
        Session session = this.getCurrentSession();
        session.beginTransaction();
        session.delete(obj);
        session.getTransaction().commit();
    }
    
}

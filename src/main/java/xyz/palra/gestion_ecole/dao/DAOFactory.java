/*
 * Copyright (C) 2017 palra
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xyz.palra.gestion_ecole.dao;

import java.sql.Connection;
import org.hibernate.Session;

/**
 * Construit des instances de DAO
 * @author palra
 */
public class DAOFactory {
    private Session session;

    public DAOFactory(Session session) {
        this.session = session;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }
    
    /**
     * Retourne une instance du DAO des classes.
     * @return L'instance du DAO des classes.
     */
    public ClasseDAO getClasseDAO() {
        return new ClasseDAO(session);
    }
    
    public MatiereDAO getMatiereDAO() {
        return new MatiereDAO(session);
    }
    
    public EvaluationDAO getEvaluationDAO() {
        return new EvaluationDAO(session);
    }
    
    public EleveDAO getEleveDAO() {
        return new EleveDAO(session);
    }
}

/*
 * Copyright (C) 2017 palra
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xyz.palra.gestion_ecole.dao;

import java.util.List;
import java.util.Map;

/**
 *
 * @author palra
 */
public interface DAOInterface<T> {
    
    /**
     * Récupère la liste de tous les objets de type T en base.
     * @return La liste de toutes les entités de classe T
     */
    public abstract List<T> findAll();

    /**
     * Construit une requête et renvoie l'entité de type T correspondante, si 
     * trouvée.
     * 
     * @param id L'ID de l'entité à trouver
     * @return L'entité à l'ID spécifié, null si il n'a pas été trouvé
     */
    public abstract T findById(int id);
    
    /**
     * Persiste en base de données un objet de type T. Si la base de données
     * a généré automatiquement de nouvelles valeurs (id, autoincrement ...), 
     * doit également faire correspondre ces changements dans l'objet passé en
     * paramètre.
     * @param obj L'objet à persister 
     * @return L'objet persisté
     */
    public abstract T create(T obj);

    /**
     * Met à jour en base de données un objet de type T. Si la base de données
     * a généré automatiquement de nouvelles valeurs (id, autoincrement ...), 
     * doit également faire correspondre ces changements dans l'objet passé en
     * paramètre.
     * @param obj L'objet à modifier
     * @return L'objet modifié
     */
    public abstract T update(T obj);
    
    /**
     * Supprime un objet de type T de la base de données. Si l'objet contient 
     * des valeurs issues de la base de données (id, autoincrement ...), elles
     * doivent être supprimées de l'objet référé.
     * @param obj L'objet à supprimer
     */
    public abstract void delete(T obj);
    
}

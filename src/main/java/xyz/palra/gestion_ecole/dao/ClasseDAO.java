/*
 * Copyright (C) 2017 palra
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xyz.palra.gestion_ecole.dao;

import java.sql.Connection;
import java.util.*;
import javax.management.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import xyz.palra.gestion_ecole.mock.ClasseMockData;
import xyz.palra.gestion_ecole.mock.EleveMockData;
import xyz.palra.gestion_ecole.model.Classe;
import xyz.palra.gestion_ecole.model.Eleve;

/**
 * DAO de gestion des classes.
 */
public class ClasseDAO extends AbstractDAO<Classe> {

    public ClasseDAO(Session session) {
        super(session);
    }

    @Override
    public List<Classe> findAll() {
        List<Classe> result;
        Session session = this.getCurrentSession();
        session.beginTransaction();
        result = session.createQuery( "SELECT c FROM Classe c", Classe.class ).list();
        session.getTransaction().commit();
        
        return result;
    }
    
    @Override
    public Classe findById(int id) {
        Session session = this.getCurrentSession();
        session.beginTransaction();
        Classe classe = (Classe) session.get(Classe.class,id);
        session.getTransaction().commit();
        
        return classe;
    }
    
    public List<Eleve> findEleves(Classe obj) {
        Session session = this.getCurrentSession();
        session.beginTransaction();
        List<Eleve> result = session.createQuery( "SELECT e FROM Eleve e where e.classe=:classe" ).setParameter("classe", obj.getId()).list();
        session.getTransaction().commit();
        
        return result;
    }

    @Override
    public Classe create(Classe obj) {
        Session session = this.getCurrentSession();
        session.beginTransaction();
        session.save(obj);
        session.getTransaction().commit();
                
        return obj;
    }

    @Override
    public Classe update(Classe obj) {
        Session session = this.getCurrentSession();
        session.beginTransaction();
        session.update(obj);
        session.getTransaction().commit();
        
        return obj;
    }

    @Override
    public void delete(Classe obj) {
        Session session = this.getCurrentSession();
        session.beginTransaction();
        session.delete(obj);
        session.getTransaction().commit();
    }

}
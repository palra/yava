/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.palra.gestion_ecole.dao;

import java.util.List;
import org.hibernate.Session;
import xyz.palra.gestion_ecole.model.Evaluation;

/**
 *
 * @author alexis
 */
public class EvaluationDAO extends AbstractDAO<Evaluation> {
    
    public EvaluationDAO(Session session) {
        super(session);
    }

    @Override
    public List<Evaluation> findAll() {
        List<Evaluation> result;
        Session session = this.getCurrentSession();
        session.beginTransaction();
        result = session.createQuery( "SELECT e FROM Evaluation e", Evaluation.class ).list();
        session.getTransaction().commit();
        
        return result;
    }
    
    @Override
    public Evaluation findById(int id) {
        Session session = this.getCurrentSession();
        session.beginTransaction();
        Evaluation evaluation = (Evaluation) session.get(Evaluation.class,id);
        session.getTransaction().commit();
        
        return evaluation;
    }

    @Override
    public Evaluation create(Evaluation obj) {
        Session session = this.getCurrentSession();
        session.beginTransaction();
        session.save(obj);
        session.getTransaction().commit();
                
        return obj;
    }

    @Override
    public Evaluation update(Evaluation obj) {
        Session session = this.getCurrentSession();
        session.beginTransaction();
        session.update(obj);
        session.getTransaction().commit();      
        
        return obj;
    }

    @Override
    public void delete(Evaluation obj) {
        Session session = this.getCurrentSession();
        session.beginTransaction();
        session.delete(obj);
        session.getTransaction().commit();
    }
}

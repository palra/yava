/*
 * Copyright (C) 2017 palra
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xyz.palra.gestion_ecole.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import xyz.palra.gestion_ecole.dao.DAOInterface;

/**
 * Objet d'accès aux données abstrait.
 * Classe paramétrée, le type T correspond à la classe à gérer.
 */
public abstract class AbstractDAO<T> implements DAOInterface<T> {

    private final Session currentSession;
    
    /**
     * Construit une DAO en injectant la currentSession courante
     * @param session La session courante
     */
    public AbstractDAO(Session session) {
        this.currentSession = session;
    }

    public Session getCurrentSession() {
        return currentSession;
    }
}
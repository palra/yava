/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.palra.gestion_ecole.repository;

import java.util.List;
import xyz.palra.gestion_ecole.security.UtilisateurRepositoryInterface;
import org.hibernate.Session;
import xyz.palra.gestion_ecole.model.Utilisateur;
import xyz.palra.gestion_ecole.security.UtilisateurInterface;

/**
 *
 * @author palra
 */
public class UtilisateurRepository implements UtilisateurRepositoryInterface {
    private Session session;

    public UtilisateurRepository(Session session) {
        this.session = session;
    }

    @Override
    public UtilisateurInterface findByEmailPassword(String email, String password) {
        session.beginTransaction();
        
        List<Utilisateur> users = session
                .createQuery("SELECT u FROM Utilisateur u WHERE u.email = :email AND u.password = :password", Utilisateur.class)
                .setParameter("email", email)
                .setParameter("password", password)
                .list();
        session.getTransaction().commit();
        
        Utilisateur user = users.isEmpty() ? null : users.get(0);
        
        return user;
    }
    
}
